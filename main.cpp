#include <iostream>
#include <string>
#include <fstream>

using namespace std;

uint32_t BELLOW_TWENTY[] = {
        3,  // "one",
        3,  // "two",
        5,  // "three",
        4,  // "four",
        4,  // "five",
        3,  // "six",
        5,  // "seven",
        5,  // "eight",
        4,  // "nine",
        3,  // "ten",
        6,  // "eleven",
        6,  // "twelve",
        8,  // "thirteen",
        8,  // "fourteen",
        7,  // "fifteen",
        7,  // "sixteen",
        9,  // "seventeen",
        8,  // "eighteen",
        8,  // "nineteen",
        6,  // "twenty"
};

uint32_t TENS[] = {
        6, // "twenty",
        6, // "thirty",
        5, // "forty",
        5, // "fifty",
        5, // "sixty",
        7, // "seventy",
        6, // "eighty",
        6, // "ninety"
};

uint32_t length(uint32_t x) {
    if (x <= 20) {
        return BELLOW_TWENTY[x-1];
    } else {
        uint32_t y = x % 10;

        if (y > 0) {
            return TENS[(x / 10) - 2] + length(y);
        } else {
            return TENS[(x / 10) - 2];
        }

    }
}

uint32_t normal_sum(uint32_t a, uint32_t b, uint32_t c) {
    return a + b + c;
}

uint32_t letter_sum(uint32_t a, uint32_t b, uint32_t c) {
    return length(a) + length(b) + length(c);
}

struct Square {
    uint32_t values[3][3];

    Square(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e, uint32_t f, uint32_t g, uint32_t h, uint32_t i) {
        values[0][0] = a;
        values[0][1] = b;
        values[0][2] = c;

        values[1][0] = d;
        values[1][1] = e;
        values[1][2] = f;

        values[2][0] = g;
        values[2][1] = h;
        values[2][2] = i;
    }

    bool is_magic() {
        uint32_t sum = values[0][0] + values[1][1] + values[2][2];

        if (sum != values[2][0] + values[1][1] + values[0][2]) return false;

        for (int i = 0; i < 3; ++i) {
            if (sum != values[i][0] + values[i][1] + values[i][2]) return false;
        }

        for (int i = 0; i < 3; ++i) {
            if (sum != values[0][i] + values[1][i] + values[2][i]) return false;
        }

        return true;
    }

    bool is_letterwise_magic() {
        uint32_t sum = letter_sum(values[0][0], values[1][1], values[2][2]);

        if (sum != letter_sum(values[2][0], values[1][1], values[0][2])) return false;

        for (int i = 0; i < 3; ++i) {
            if (sum != letter_sum(values[i][0], values[i][1], values[i][2])) return false;
        }

        for (int i = 0; i < 3; ++i) {
            if (sum != letter_sum(values[0][i], values[1][i], values[2][i])) return false;
        }

        return true;
    }

    void save() {
        ofstream file;
        file.open("results/" + to_string(values[0][0]) + "_" + to_string(values[0][1]) + "_" + to_string(values[0][2]) + "_"
                             + to_string(values[1][0]) + "_" + to_string(values[1][1]) + "_" + to_string(values[1][2]) + "_"
                             + to_string(values[2][0]) + "_" + to_string(values[2][1]) + "_" + to_string(values[2][2]));

        for (int i = 0; i < 3; ++i) {
            file << values[i][0] << " " << values[i][1] << " " << values[i][2] << endl;
        }

        file.close();

    }
};

/*
 * a b c
 * d e f
 * g h i
 */

int main () {
    for (uint32_t a = 1; a < 100; ++a) {

        for (uint32_t b = 1; b < 100; ++b) {
            if (b == a) continue;

            for (uint32_t c = 1; c < 100; ++c) {
                if (c == a or c == b) continue;

                uint32_t row_0_normal = normal_sum(a, b, c);
                uint32_t row_0_letter = letter_sum(a, b, c);

                cout << "First row " << a << " " << b << " " << c << endl;

                for (uint32_t d = 1; d < 100; ++d) {
                    if (d == a or d == b or d == c) continue;

                    for (uint32_t g = 1; g < 100; ++g) {
                        if (g == a or g == b or g == c or g == d) continue;

                        if (row_0_normal != normal_sum(a, d, g)) continue;
                        if (row_0_letter != letter_sum(a, d, g)) continue;

                        for (uint32_t e = 1; e < 100; ++e) {
                            if (e == a or e == b or e == c or e == d or e == g) continue;

                            for (uint32_t i = 1; i < 100; ++i) {
                                if (i == a or i == b or i == c or i == d or i == g or i == e) continue;

                                if (row_0_normal != normal_sum(a, e, i)) continue;
                                if (row_0_letter != letter_sum(a, e, i)) continue;

                                for (uint32_t f = 1; f < 100; ++f) {
                                    if (f == a or f == b or f == c or f == d or f == g or f == e or f == i) continue;

                                    if (row_0_normal != normal_sum(d, e, f)) continue;
                                    if (row_0_letter != letter_sum(d, e, f)) continue;

                                    for (uint32_t h = 1; h < 100; ++h) {
                                        if (h == a or h == b or h == c or h == d or h == g or h == e or h == i or h == f) continue;

                                        Square square = Square(a, b, c, d, e, f, g, h, i);
                                        if (square.is_magic()) {
                                            if (square.is_letterwise_magic()) {
                                                square.save();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return 0;
}