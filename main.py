# encoding=utf-8
import sys

## Oh Boi this is really bad


def generate_squares(start, end):
    """ This is really ugly, but it generates all squares that have unique members """
    for a in range(start, end):
        print("A is at {}".format(a))
        for b in range(1, 100):
            if b == a:
                continue
            print("B is at {}".format(b))
            for c in range(1, 100):
                if c in {a, b}:
                    continue
                for d in range(1, 100):
                    if d in {a, b, c}:
                        continue
                    for e in range(1, 100):
                        if e in {a, b, c, d}:
                            continue
                        for f in range(1, 100):
                            if f in {a, b, c, d, e}:
                                continue
                            for g in range(1, 100):
                                if g in {a, b, c, d, e, f}:
                                    continue
                                for h in range(1, 100):
                                    if h in {a, b, c, d, e, f, g}:
                                        continue
                                    for i in range(1, 100):
                                        if i in {a, b, c, d, e, f, g, h}:
                                            continue
                                        yield [
                                            [a, b, c],
                                            [d, e, f],
                                            [g, h, i]
                                        ]


def is_square_magic(square, func=lambda x: x):
    sum = func(square[0][0]) + func(square[1][1]) + func(square[2][2])
    sum2 = func(square[2][0]) + func(square[1][1]) + func(square[0][2])

    if sum != sum2:
        return False

    for i in range(0, 2):
        sum2 = func(square[i][0]) + func(square[i][1]) + func(square[i][2])
        if sum != sum2:
            return False

    for i in range(0, 2):
        sum2 = func(square[0][i]) + func(square[1][i]) + func(square[2][i])
        if sum != sum2:
            return False

    return True


BELLOW_TWENTY = [
    3,  # "one",
    3,  # "two",
    5,  # "three",
    4,  # "four",
    4,  # "five",
    3,  # "six",
    5,  # "seven",
    5,  # "eight",
    4,  # "nine",
    3,  # "ten",
    6,  # "eleven",
    6,  # "twelve",
    8,  # "thirteen",
    8,  # "fourteen",
    7,  # "fifteen",
    7,  # "sixteen",
    9,  # "seventeen",
    8,  # "eighteen",
    8,  # "nineteen",
    6,  # "twenty"
]

TENS = [
    6, # "twenty",
    6, # "thirty",
    5, # "forty",
    5, # "fifty",
    5, # "sixty",
    7, # "seventy",
    6, # "eighty",
    6, # "ninety"
]


def letterwise_length(x):
    if x <= 20:
        return BELLOW_TWENTY[x-1]
    else:
        y = x % 10

        return TENS[(x // 10) - 2] + letterwise_length(y) if y else 0


def print_square(square):
    print(square[0][0], square[0][1], square[0][2])
    print(square[1][0], square[1][1], square[1][2])
    print(square[2][0], square[2][1], square[2][2])
    print()

def save_square(square):
    with open("results/{}".format("_".join([str(item) for sublist in square for item in sublist])), "w") as fl:
        fl.write("{}\n{}\n{}\n".format(*["{} {} {}".format(*line) for line in square]))

if __name__ == "__main__":
    for square in generate_squares(int(sys.argv[1]), int(sys.argv[2])):
        if is_square_magic(square):
            print_square(square)
            if is_square_magic(square, letterwise_length):
                print_square(square)
                save_square(square)

