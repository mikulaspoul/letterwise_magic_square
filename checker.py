import os

BELLOW_TWENTY = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
    "eleven",
    "twelve",
    "thirteen",
    "fourteen",
    "fifteen",
    "sixteen",
    "seventeen",
    "eighteen",
    "nineteen",
    "twenty"
]

TENS = [
    "twenty",
    "thirty",
    "forty",
    "fifty",
    "sixty",
    "seventy",
    "eighty",
    "ninety"
]

def english(x):
    if x <= 20:
        return BELLOW_TWENTY[x-1]
    else:
        y = x % 10

        if y:
            return TENS[(x // 10) - 2] + (" " + english(y)) if y else ""
        else:
            return TENS[(x // 10) - 2]


def letter_length(x):
    return len(english(x).replace(" ", ""))

def is_square_magic(square, func=lambda x: x):
    sum = func(square[0][0]) + func(square[1][1]) + func(square[2][2])
    sum2 = func(square[2][0]) + func(square[1][1]) + func(square[0][2])

    if sum != sum2:
        return False

    for i in range(0, 2):
        sum2 = func(square[i][0]) + func(square[i][1]) + func(square[i][2])
        if sum != sum2:
            return False

    for i in range(0, 2):
        sum2 = func(square[0][i]) + func(square[1][i]) + func(square[2][i])
        if sum != sum2:
            return False

    return True


def check_results(name):

    square = []
    with open(name) as fl:
        for line in fl:
            if not line.strip():
                continue
            try:
                square.append([int(x) for x in line.strip().split(" ")])
            except:
                print("problem: ", name)
                raise

    try:
        if not is_square_magic(square) or not is_square_magic(square, letter_length):
            return False
    except:
        return False
    return True

if __name__ == "__main__":
    for name in os.listdir("results"):
        if not check_results("results/" + name):
            print(name, "does not match")
        else:
            print(name, "does check out")
