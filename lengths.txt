# the numbers used, left is checked with the english representation and the lentgh, right is the length from c++

1 one 3                     1 : 3
2 two 3                     2 : 3
3 three 5                   3 : 5
4 four 4                    4 : 4
5 five 4                    5 : 4
6 six 3                     6 : 3
7 seven 5                   7 : 5
8 eight 5                   8 : 5
9 nine 4                    9 : 4
10 ten 3                    10 : 3
11 eleven 6                 11 : 6
12 twelve 6                 12 : 6
13 thirteen 8               13 : 8
14 fourteen 8               14 : 8
15 fifteen 7                15 : 7
16 sixteen 7                16 : 7
17 seventeen 9              17 : 9
18 eighteen 8               18 : 8
19 nineteen 8               19 : 8
20 twenty 6                 20 : 6
21 twenty one 9             21 : 9
22 twenty two 9             22 : 9
23 twenty three 11          23 : 11
24 twenty four 10           24 : 10
25 twenty five 10           25 : 10
26 twenty six 9             26 : 9
27 twenty seven 11          27 : 11
28 twenty eight 11          28 : 11
29 twenty nine 10           29 : 10
30 thirty 6                 30 : 6
31 thirty one 9             31 : 9
32 thirty two 9             32 : 9
33 thirty three 11          33 : 11
34 thirty four 10           34 : 10
35 thirty five 10           35 : 10
36 thirty six 9             36 : 9
37 thirty seven 11          37 : 11
38 thirty eight 11          38 : 11
39 thirty nine 10           39 : 10
40 forty 5                  40 : 5
41 forty one 8              41 : 8
42 forty two 8              42 : 8
43 forty three 10           43 : 10
44 forty four 9             44 : 9
45 forty five 9             45 : 9
46 forty six 8              46 : 8
47 forty seven 10           47 : 10
48 forty eight 10           48 : 10
49 forty nine 9             49 : 9
50 fifty 5                  50 : 5
51 fifty one 8              51 : 8
52 fifty two 8              52 : 8
53 fifty three 10           53 : 10
54 fifty four 9             54 : 9
55 fifty five 9             55 : 9
56 fifty six 8              56 : 8
57 fifty seven 10           57 : 10
58 fifty eight 10           58 : 10
59 fifty nine 9             59 : 9
60 sixty 5                  60 : 5
61 sixty one 8              61 : 8
62 sixty two 8              62 : 8
63 sixty three 10           63 : 10
64 sixty four 9             64 : 9
65 sixty five 9             65 : 9
66 sixty six 8              66 : 8
67 sixty seven 10           67 : 10
68 sixty eight 10           68 : 10
69 sixty nine 9             69 : 9
70 seventy 7                70 : 7
71 seventy one 10           71 : 10
72 seventy two 10           72 : 10
73 seventy three 12         73 : 12
74 seventy four 11          74 : 11
75 seventy five 11          75 : 11
76 seventy six 10           76 : 10
77 seventy seven 12         77 : 12
78 seventy eight 12         78 : 12
79 seventy nine 11          79 : 11
80 eighty 6                 80 : 6
81 eighty one 9             81 : 9
82 eighty two 9             82 : 9
83 eighty three 11          83 : 11
84 eighty four 10           84 : 10
85 eighty five 10           85 : 10
86 eighty six 9             86 : 9
87 eighty seven 11          87 : 11
88 eighty eight 11          88 : 11
89 eighty nine 10           89 : 10
90 ninety 6                 90 : 6
91 ninety one 9             91 : 9
92 ninety two 9             92 : 9
93 ninety three 11          93 : 11
94 ninety four 10           94 : 10
95 ninety five 10           95 : 10
96 ninety six 9             96 : 9
97 ninety seven 11          97 : 11
98 ninety eight 11          98 : 11
99 ninety nine 10           99 : 10
